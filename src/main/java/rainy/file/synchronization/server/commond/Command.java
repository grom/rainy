package rainy.file.synchronization.server.commond;

public class Command<T> {
	public String commondType;

	public String remoteAddress;

	private T body;

	private T fullContent;

	public T getBody() {
		return body;
	}

	public void setBody(T body) {
		this.body = body;
	}

	public T getFullContent() {
		return fullContent;
	}

	public void setFullContent(T fullContent) {
		this.fullContent = fullContent;
	}

	public String getRemoteAddress() {
		return remoteAddress;
	}

	public void setRemoteAddress(String remoteAddress) {
		this.remoteAddress = remoteAddress;
	}
}
